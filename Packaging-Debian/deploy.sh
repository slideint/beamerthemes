#!/bin/bash

# memorise the current working directory
oldrep=${PWD}
echo Give the version name of the package \(e.g. YearMonthDay\)
read x
# in the directory debian of beamerthemes
cd ${HOMEBEAMERTHEMES}/Packaging-Debian/
# launch the script that creates the package
./beamerthemes_package.sh ${x} ${HOMEBEAMERTHEMES}/APT_Debian_repository
# in directory /tmp
cd /tmp
cp beamerthemes*.changes ${HOMEBEAMERTHEMES}/APT_Debian_repository/sources/
cp beamerthemes*.dsc ${HOMEBEAMERTHEMES}/APT_Debian_repository/binary/
find ${HOMEBEAMERTHEMES}/APT_Debian_repository -type d -exec chmod a+rx {} \;
find ${HOMEBEAMERTHEMES}/APT_Debian_repository -type f -exec chmod a+r {} \;
echo Update and deployment done.
ls -lR ${HOMEBEAMERTHEMES}/APT_Debian_repository/
mv ${HOMEBEAMERTHEMES}/Packaging-Debian/changelog ${HOMEBEAMERTHEMES}/Packaging-Debian/changelog.old
mv /tmp/changelog ${HOMEBEAMERTHEMES}/Packaging-Debian
echo END
# return to the working directory at the beginning of the script
cd ${oldrep}
