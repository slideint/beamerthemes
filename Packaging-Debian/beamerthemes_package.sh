#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Usage $0 version repository"
    echo "where  version is in the form yearmonthday"
    echo "and repository is the directory in which bin packages will be stored"
    exit 1
fi
# $1 is the name of the version, in the form yearmonthday
# $2 is the location of the apt repository where to deploy the package
echo '$1' = $1
echo '$2' = $2
# even if the directory debian is not part of the package, export it
rm -rf /tmp/beamerthemes-*
cp -r ${HOMEBEAMERTHEMES}/sources /tmp/beamerthemes-$1
cp -r ${HOMEBEAMERTHEMES}/Packaging-Debian /tmp/beamerthemes-$1
mv /tmp/beamerthemes-$1/Packaging-Debian /tmp/beamerthemes-$1/debian
cd /tmp/beamerthemes-$1/debian
pwd
# fill in the new version change log
dch -v $1
cp changelog /tmp
cd ..
# -us: non signed source package; -uc: non signed file .changes
dpkg-buildpackage -rfakeroot
rm -f $2/binary/beamerthemes_*_all.deb
cp ../beamerthemes_$1_all.deb $2/binary/
rm -f $2/sources/beamerthemes_*
cp ../beamerthemes_$1.tar.gz ../beamerthemes_$1.dsc $2/sources/
cd $2
dpkg-scanpackages binary /dev/null | gzip -9c > binary/Packages.gz
chmod a+r binary/*
dpkg-scansources sources /dev/null | gzip -9c > sources/Sources.gz
chmod a+r sources/*
