Format: 1.0
Source: beamerthemes
Binary: beamerthemes
Architecture: all
Version: 20180318
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 beamerthemes deb unknown extra arch=all
Checksums-Sha1:
 76e62d7ea954b5da24f3506bc144bb8ea6e95cb9 4698443 beamerthemes_20180318.tar.gz
Checksums-Sha256:
 e752026a56d69621e5342b548776f3be7b94edd1304f761d28ff8eadc32fed97 4698443 beamerthemes_20180318.tar.gz
Files:
 92b39cad221df04d76cf96cb43082e6b 4698443 beamerthemes_20180318.tar.gz
