Format: 1.0
Source: beamerthemes
Binary: beamerthemes
Architecture: all
Version: 20170703
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 beamerthemes deb unknown extra arch=all
Checksums-Sha1:
 d0af20a266b9ab8bb80319f1282dde95a22ce67f 4464694 beamerthemes_20170703.tar.gz
Checksums-Sha256:
 aedb93bdfa8f996bafef90a276522ee7c76807ab37b540faa7b9a81a553951c4 4464694 beamerthemes_20170703.tar.gz
Files:
 acbc3f3730d796c1a115890386cb0313 4464694 beamerthemes_20170703.tar.gz
