Format: 1.0
Source: beamerthemes
Binary: beamerthemes
Architecture: all
Version: 20190822
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 beamerthemes deb unknown extra arch=all
Checksums-Sha1:
 286df7115c691e2221870c730a3efcd1926c8382 2550564 beamerthemes_20190822.tar.gz
Checksums-Sha256:
 ee7e0ad5c075dc2f84df75e5592737a6a2edab54af2aa032a0e9420b2f6d31c4 2550564 beamerthemes_20190822.tar.gz
Files:
 1763a885c6d8f8bca5a5ffe2eb1e890a 2550564 beamerthemes_20190822.tar.gz
