Format: 1.0
Source: beamerthemes
Binary: beamerthemes
Architecture: all
Version: 20160711
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 beamerthemes deb unknown extra arch=all
Checksums-Sha1:
 eef047f3baff8dddb8821b1ade0a66cefd72c5d4 4463067 beamerthemes_20160711.tar.gz
Checksums-Sha256:
 115f6a6c984a8fd9b2803a60e38380b849e26624e96fd6d56eb41c40e122fd28 4463067 beamerthemes_20160711.tar.gz
Files:
 3dbf995e169a849f103fa84995528141 4463067 beamerthemes_20160711.tar.gz
