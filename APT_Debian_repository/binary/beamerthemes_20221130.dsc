Format: 1.0
Source: beamerthemes
Binary: beamerthemes
Architecture: all
Version: 20221130
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 10)
Package-List:
 beamerthemes deb unknown extra arch=all
Checksums-Sha1:
 eaedcbc339e25f9df9147d3a7acb09b380a97c0a 2554567 beamerthemes_20221130.tar.gz
Checksums-Sha256:
 66013f71549a52b41ed69b46504fa172b782ad1ae5ac4f231dcef69a8b511a16 2554567 beamerthemes_20221130.tar.gz
Files:
 361391dadfaf6b6b39fc1f45bc8e52ac 2554567 beamerthemes_20221130.tar.gz
