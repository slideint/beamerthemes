Format: 1.0
Source: beamerthemes
Binary: beamerthemes
Architecture: all
Version: 20200130
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 beamerthemes deb unknown extra arch=all
Checksums-Sha1:
 b1779dd8f09c22ec9fbc498f58e272b890fdf7ff 2720424 beamerthemes_20200130.tar.gz
Checksums-Sha256:
 fb5ebc0e76cf556b9268f1db3ffb1d3e4727a62e46efa231ddd725f10b835c15 2720424 beamerthemes_20200130.tar.gz
Files:
 e68046e7639bc7883739d1a48a55b00b 2720424 beamerthemes_20200130.tar.gz
