Format: 1.0
Source: beamerthemes
Binary: beamerthemes
Architecture: all
Version: 20160711-2
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Package-List:
 beamerthemes deb unknown extra arch=all
Checksums-Sha1:
 5f2b369b4aab472d5f2dcea1eea5e02f83fd4966 4463574 beamerthemes_20160711-2.tar.gz
Checksums-Sha256:
 c092e8194a3447ae22e3c1e6200d0ace0ddd50fa5cd4b545bf547eb981625280 4463574 beamerthemes_20160711-2.tar.gz
Files:
 6376514c8fb74199129fe6937c7b371e 4463574 beamerthemes_20160711-2.tar.gz
