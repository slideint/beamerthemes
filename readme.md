# LaTeX2e beamer themes

This project proposes a new beamer theme for presenting slides for
Télécom SudParis.

Note: The graphic layout of the theme is up to date w.r.t. the
internal official guidelines of Télécom SudParis (including the
Université Paris-Saclay logo).

## Contents

This repository contains beamer themes for Télécom SudParis.

Unless otherwise authorized, this material should only be used in the
internal context of Telecom SudParis, and isn't meant for public
redistribution.

In particular, the logos and other visual elements may be governed by
copyright and/or trademark rights of their respective owners.


### Content of this directory:

- `LICENSE.txt`          : Creative Commons Attribution 4.0 International
- `readme.md`            : this file
- `APT_Debian_repository`: the scripts and the data of the debian packaging
- `sources`              : the sources of the themes and the examples

## Installation

### Installation from the debian repository

A scenario with commands is provided at the end of this section.

Debian packages are available on directory  [APT_Debian_repository](./APT_Debian_repository/). For instance, Debian package file [APT_Debian_repository/binary/beamerthemes_20221130_all.deb](APT_Debian_repository/binary/beamerthemes_20221130_all.deb).

The dependencies of the Debian package are listed in file [./Packaging-Debian/control](./Packaging-Debian/control).

Practically, execute the following commands:
```
$ 
...
Either manually or using wget, download the deb file https://gitlab.inf.telecom-sudparis.eu/slideint/beamerthemes/-/blob/master/APT_Debian_repository/binary/beamerthemes_20221130_all.deb
...
$ sudo apt-get install -f ./beamerthemes_20221130_all.deb
...
Check the installation
$ kpsewhich beamerthemetsp.sty
/usr/local/share/texmf/tex/latex/beamer/themes/theme/beamerthemetsp.sty
$ kpsewhich beamercolorthemetsp.sty 
/usr/local/share/texmf/tex/latex/beamer/themes/color/beamercolorthemetsp.sty
$ kpsewhich beamerfontthemetsp.sty
/usr/local/share/texmf/tex/latex/beamer/themes/font/beamerfontthemetsp.sty
$ kpsewhich beamerinnerthemetsp.sty
/usr/local/share/texmf/tex/latex/beamer/themes/inner/beamerinnerthemetsp.sty
$ kpsewhich beamerouterthemetsp.sty
/usr/local/share/texmf/tex/latex/beamer/themes/outer/beamerouterthemetsp.sty
...
```
    
### Installation from the sources


```
...
$ cd /tmp
$ git clone ssh://git@gitlab.inf.telecom-sudparis.eu:2222/slideint/beamerthemes.git
$ cd beamerthemes/sources
$ cp -r texmf/ ~/
...
Check the installation
$ kpsewhich beamerthemetsp.sty
~/texmf/tex/latex/beamer/themes/theme/beamerthemetsp.sty
$ kpsewhich beamercolorthemetsp.sty 
~/texmf/tex/latex/beamer/themes/color/beamercolorthemetsp.sty
$ kpsewhich beamerfontthemetsp.sty
~/texmf/tex/latex/beamer/themes/font/beamerfontthemetsp.sty
$ kpsewhich beamerinnerthemetsp.sty
~/texmf/tex/latex/beamer/themes/inner/beamerinnerthemetsp.sty
$ kpsewhich beamerouterthemetsp.sty
~/texmf/texmf/tex/latex/beamer/themes/outer/beamerouterthemetsp.sty
...
```

## Usages

See the file `sources/texlive-doc/.../conference-talks/conference-ornate-20min.tex`

```
\documentclass{beamer}
\mode<presentation>; {
  \usetheme{tsp}
  \setbeamercovered{transparent}
}
```

## Known bugs

None reported.
But presumably some.

## Changes

* 2022/11/30: reorganise directories and generate a new debian package.

* 2022/03/07: change repisotory and reorganise repository.

* 2019/08/22: the new logo of Telecom SudParis with IP Paris.

* 2018/03/18: add logo of Telecom Evolution.

* 2016/07/11: reorganise repository.

* 2015/06/08: fix bug on long frame titles breaking headline.

* 2015/06/04: first release.
